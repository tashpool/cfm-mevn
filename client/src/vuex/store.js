import Vue from 'vue'
import Vuex from 'vuex'
import * as snackbar from './modules/snackbar'
import * as CFMService from '@/services/CFMService'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    snackbar
  },
  state: {
    user: null,
    currentCaseID: null,
    currentEventID: null,
    cachedActors: []
  },
  mutations: {
    SET_USER_DATA(state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
      // CFMService.axios_instance.defaults.headers.common['Authorization'] = `Bearer ${userData.token}`
    },
    CLEAR_USER_DATA() {
      localStorage.removeItem('user')
      location.reload()
    },
    SET_USER_STATE(state) {
      state.user = JSON.parse(localStorage.getItem('user'))
    },
    SET_CASE_ID(state, newCaseID) {
      state.currentCaseID = newCaseID
    },
    SET_EVENT_ID(state, newEventID) {
      state.currentEventID = newEventID
    },
    SET_ACTOR_CACHE(state, actorArray) {
      state.cachedActors = actorArray
    },
    CLEAR_ACTOR_CACHE(state) {
      // state.cachedActors.length = 0
      state.cachedActors.splice(0)
    }
  },
  actions: {
    signup({ commit }, credentials) {
      return CFMService.signupUser(credentials)
        .then(({ data }) => {
          commit('SET_USER_DATA', data)
        })
        .catch(err => {
          return err.response
        })
    },
    login({ commit }, credentials) {
      return CFMService.loginUser(credentials)
        .then(({data}) => {
          commit('SET_USER_DATA', data)
        })
        .catch(err => {
          console.log('Axios Error: ', err.response)
          return err.response
        })
    },
    logout({ commit }) {
      commit('CLEAR_USER_DATA')
    },
    checkState({ commit, state }) {
      // We have user info in web storage, but not in the vuex state
      try {
        if (localStorage.getItem('user') && !state.user) {
          commit('SET_USER_STATE')
        }
      } catch(err) {
        console.log('Axios Error: ', err.response)
        return err.response
      }
    },
    // Save ObjectID from calling page for return path
    setCaseID({ commit }, newCaseID) {
      commit('SET_CASE_ID', newCaseID)
    },
    setEventID({ commit }, newEventID) {
      commit('SET_EVENT_ID', newEventID)
    },
    cacheActors({ commit }, caseID) {
      // Get all events
      // For each event, push on actor[]
      return CFMService.getCaseActors(caseID)
        .then(({data}) => {
          commit('SET_ACTOR_CACHE', data.data.actors)
        })
        .catch(err => {
          console.log('Axios Error: ', err.response)
          return err.response
        })
    },
    clearCachedActors({commit}) {
      commit('CLEAR_ACTOR_CACHE')
    }
  },
  getters: {
    loggedIn(state) {
      return !!state.user
    },
    getCaseID(state) {
      return state.currentCaseID
    },
    getEventID(state) {
      return state.currentEventID
    },
    updateActors(state) {
      return state.cachedActors.length < 1
    },
    getCaseActors(state) {
      return state.cachedActors
    }
  }
})
export const state = {
  message: null,
  color: null
}

export const mutations = {
  SHOW_MESSAGE(state, payload) {
    state.message = payload.message
    state.color = payload.color
  }
}

export const actions = {
  showMessage({ commit }, payload) {
    // any checks needed here?
    commit('SHOW_MESSAGE', payload)
  }
}
// import Api from '../services/Api'
import axios from 'axios'
import store from '../vuex/store'
import FormData from 'form-data'

export const axios_instance = axios.create({
  baseURL: 'http://localhost:8081/api',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

axios_instance.interceptors.request.use((config) => {
  console.log('CFMService: Checking for logged in user...')
  if (store.getters.loggedIn) {
    // console.log(`Found token, setting req.headers.auth...`)
    config.headers.authorization = `Bearer ${store.state.user.token}`
    // axios_instance.defaults.headers.common['Authorization'] = `Bearer ${store.state.user.token}`
  }

  return config
})
// axios_instance.interceptors.request.use(req => {
//   console.log(`${req.method}: ${req.url}`)
//   return req
// })
// axios_instance.interceptors.response.use(res => {
//   console.log('Axios Resp: ', res.data.json)
//   console.log('Headers: ', res.headers)
//   return res
// })

  // -=-=-=-=-=-= User
  export function loginUser(credentials) {
    return axios_instance.post('/users/login', credentials)
  }

  export function signupUser( userData ) {
    return axios_instance.post('/users/signup', userData)
  }

  // Only updating user image at the moment
  export function updateMe( userData ) {
    const form = new FormData()

    form.append('photo', userData.photo)

    return axios_instance.patch('/users/updateMe',
      form,
      { headers: {'Content-Type': 'multipart/form-data'} }
      )
  }

  export function getMe() {
    return axios_instance.get('/users/me')
  }
  // signupUser( userData ) {
  //   return Api().post('/signup', {
  //     name: userData.name,
  //     email: userData.email,
  //     photo: userData.photo,
  //     password: userData.password
  //   })
  // },

  // -=-=-=-=-=-= Cases
  export function getCases(queryParams = '') {
    return axios_instance.get('/cases/' + queryParams)
  }

  export function getCase(id) {
    return axios_instance.get('/cases/' + id)
  }

  export function createCase(caseObject) {
    return axios_instance.post('/cases/', caseObject)
  }

  export function updateCase(id, caseUpdate) {
    return axios_instance.patch('/cases/' + id, caseUpdate)
  }
  // deleteCase(id) {
  //   return Api().post('/cases/' + id + '/delete')
  // }

  // -=-=-=-=-=-= Event
  export function getEvent(id) {
    return axios_instance.get('/events/' + id)
  }

  // GET has no body, limiting resources by an array of ids
  // using a post to send the JSON in the req.body
  // **Needs investigating for alternative solution**
  export function getSomeEvents(documentIDs) {
    return axios_instance.post('/events/range/', documentIDs)
  }

  // Trying to explicitly setting AUTH header, not showing up
  // after OPTIONS
  // export function createEvent(eventObject) {
  //   return axios_instance.post('/events/', eventObject, {
  //     headers: {
  //       'Authorization': `Bearer ${store.state.user.token}`
  //     }
  //   })
  // }

export function createEvent(eventObject) {
  return axios_instance.post('/events/', eventObject)
}

export function updateEvent(id, eventObject) {
  return axios_instance.patch('/events/' + id, eventObject)
}

  // -=-=-=-=-=-= Actor
  export function getActor(id) {
    return axios_instance.get('/actors/' + id)
  }

  export function getActors(queryParams = '') {
    return axios_instance.get('/actors/' + queryParams)
  }

  export function createActor(actorObject) {
    return axios_instance.post('/actors/', actorObject)
  }

export function updateActor( id, userData ) {
  const form = new FormData()

  form.append('photo', userData.photo)

  return axios_instance.patch('/actors/' + id,
    form,
    { headers: {'Content-Type': 'multipart/form-data'} }
  )
}

  // -=-=-=-=-=-= Evidence
export function getEvidence(id) {
  return axios_instance.get('/evidences/' + id)
}

export function createEvidence(evidenceObject) {
  return axios_instance.post('/evidences/', evidenceObject)
}

// -=-=-=-=-=-= Cache
export function getCaseActors(caseID) {
  return axios_instance.get('/cases/' + caseID + '/actors/')
}
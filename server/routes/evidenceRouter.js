const express = require('express')
const authController = require('./../controllers/authController')
const evidenceController = require('./../controllers/CFMEvidenceContoller')

const router = express.Router()

router.route('/')
  .get(evidenceController.cfmevidence_list)
  .post(authController.protect,
    evidenceController.cfmevidence_create)

router
  .route('/:id')
  .get(evidenceController.cfmevidence_detail)
  .patch(authController.protect,
    evidenceController.cfmevidence_update)
  .delete(authController.protect,
    evidenceController.cfmevidence_delete)

module.exports = router
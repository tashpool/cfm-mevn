let mongoose = require('mongoose')
let Schema = mongoose.Schema

let EvidenceSchema = new Schema({
  title: { type: String, required: true, max: 250 },
  category: { type: String, required: true, max: 50 },
  description: { type: String, max: 250 },
  dataURL: { type: String, max: 8000 },
  event: { type: Schema.Types.ObjectID, ref: 'Event' }
})

module.exports = mongoose.model('CFMEvidence', EvidenceSchema)
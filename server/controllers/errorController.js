const AppError = require('./../utils/appError')

const handleCastErrorDB = err => {
  const message = `Invalid ${err.path}: ${err.value}.`
  return new AppError(message, 400)
}

const handleDuplicateFieldsDB = err => {
  const value = err.errmsg.match(/(["'])(\\?.).?\1/)[0]
  const message = `Duplicate filed value ${value}. Please use another value.`
  return new AppError(message, 400)
}

const handleValidationErrorDB = err => {
  const allErrors = Object.values(err.errors).map(el => el.message)

  const message = `Invalid input data. ${allErrors.join('. ')}`
  return new AppError(message, 400)
}

const sendDevError = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack
  })
}

const sendProdError = (err, res) => {
  // Operational are expected errors, ok to send more info
  if (err.isOperational) {
    res.status(err.statusCode).json({
      status: err.status,
      message: err.message
    })
  } else {
    // logging here, but store somewhere for prod release
    console.error(`**ERROR** : ${err}`)

    res.status(500).json({
      status: 'error',
      message: 'Something when very wrong.'
    })
  }
}

module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500
  err.status = err.status || 'error'

  if (process.env.NODE_ENV === 'development') {
    sendDevError(err, res)
  } else if (process.env.NODE_ENV === 'production') {
    let error = {...err}

    if (error.name === 'CastError') error = handleCastErrorDB(error)
    if (error.code === '11000') error = handleDuplicateFieldsDB(error)
    if (error.name === 'ValidationError') error = handleValidationErrorDB(error)

    sendProdError(error, res)
  }
}
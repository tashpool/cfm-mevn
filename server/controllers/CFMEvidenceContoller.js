const CFMEvidence = require('../models/CFMEvidence')
const { body, validationResult } = require('express-validator')
handlerFactory = require('./handlerFactory')

exports.cfmevidence_list = handlerFactory.getAll(CFMEvidence)
exports.cfmevidence_detail = handlerFactory.getOne(CFMEvidence)

/*
No current check for event existence
 */
exports.cfmevidence_create = [
  body('title').isLength({ min: 1, max: 250 }).trim().escape().withMessage('Title is required.'),
  body('category').isLength({ min: 1, max: 50 }).trim().escape().withMessage('Category is required.'),

  async function(req, res, next) {
    const validationErrorChecks = validationResult(req)

    if(!validationErrorChecks.isEmpty()) {
      console.log(`Case Create Validation Check Failed`)
      const err = new Error(JSON.stringify(validationErrorChecks))
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    let newEvidence = new CFMEvidence(
      {
        title: req.body.title,
        category: req.body.category,
        description: req.body.description,
        dataURL: req.body.dataURL,
        event: req.body.event
      }
    )

    try {
      let savedEvidence = await newEvidence.save()
      res.status(201).json({
        status: "success",
        msg: 'Evidence added to system.',
        data: {
          id: savedEvidence._id
        }
      })
    } catch(tryError) {
      tryError.status = 'error'
      tryError.statusCode = 500
      return next(tryError)
    }
  }
]

exports.cfmevidence_update = handlerFactory.updateOne(CFMEvidence)
exports.cfmevidence_delete = handlerFactory.deleteOne(CFMEvidence)
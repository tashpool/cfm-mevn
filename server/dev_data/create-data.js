const fs = require('fs')
const mongoose = require('mongoose')
const dotenv = require('dotenv').config({ path: '../.env'})

const CFMEvent = require('../models/CFMEvent')
const CFMActor = require('../models/CFMActor')
const CFMCases = require('../models/CFMCases')
const CFMEvidence = require('../models/CFMEvidence')
const CFMUser = require('../models/CFMUser')

// --- Mongo Connection Setup
let mongoConnectString = 'mongodb+srv://' +
  process.env.mongoUsername +
  ':' +
  process.env.mongoPassword +
  process.env.mongoDBLoc +
  process.env.mongoDBCollection + '?retryWrites=true&w=majority'

mongoose.connect(mongoConnectString,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .catch(error => console.error(error))

let db = mongoose.connection
db.once('open', () => {
  console.log('MongoDB DB connection established successfully.')
})
db.on('error', console.error.bind(console, 'MongoDB connection error: '))
// --- End Mongo Connection Setup

const events = JSON.parse(fs.readFileSync(`${__dirname}/events.json`, 'utf-8'))
const actors = JSON.parse(fs.readFileSync(`${__dirname}/actors.json`, 'utf-8'))
const cases = JSON.parse(fs.readFileSync(`${__dirname}/cases.json`, 'utf-8'))
const evidences = JSON.parse(fs.readFileSync(`${__dirname}/evidences.json`, 'utf-8'))
const users = JSON.parse(fs.readFileSync(`${__dirname}/users.json`, 'utf-8'))

const importData = async () => {
  try {
    await CFMEvent.create(events)
    await CFMActor.create(actors)
    await CFMCases.create(cases)
    await CFMEvidence.create(evidences)
    await CFMUser.create(users, { validateBeforeSave: false })
    console.log(`Data has been imported.`)
  } catch (err) {
    console.error(err)
  }
  process.exit()
}

const deleteData = async () => {
  try {
    await CFMEvent.deleteMany()
    await CFMActor.deleteMany()
    await CFMCases.deleteMany()
    await CFMEvidence.deleteMany()
    await CFMUser.deleteMany()
    console.log(`All data deleted.`)
  } catch (err) {
    console.error(err)
  }
  process.exit()
}

if (process.argv[2] === '--import') {
  importData()
} else if (process.argv[2] === '--delete') {
  deleteData()
}
